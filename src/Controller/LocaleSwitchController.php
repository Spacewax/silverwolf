<?php
// src/Controller/LocaleSwitchController.php

namespace App\Controller;

#use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
#use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

#class LocaleSwitchController extends AbstractController {
class LocaleSwitchController extends Controller {

    /**
     * @Route("/en", name="english")
     */
    public function switchLanguageEnglish() {
        return $this->switchLanguage('en');
    }

    /**
     * @Route("/de", name="german")
     */
    public function switchLanguageGerman() {
        return $this->switchLanguage('de');
    }

    private function switchLanguage($locale) {
        $this->get('session')->set('_locale', $locale);
        return $this->redirect($this->generateUrl('home'));
    }

}
