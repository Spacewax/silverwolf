<?php
// src/Entity/Blog.php

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
     * @ORM\Entity(repositoryClass="App\Repository\BlogRepository")
     * @ORM\Table(name="blog")
     * @ORM\HasLifecycleCallbacks()
     */
    class Blog
    {
        /**
         * @ORM\Id()
         * @ORM\GeneratedValue()
         * @ORM\Column(type="integer")
         */
        private $id;

        /**
         * @ORM\Column(type="string", length=220)
         */
        private $title;

        /**
         * @ORM\Column(type="string", length=100)
         */
        private $author;

        /**
         * @ORM\Column(type="text")
         */
        private $blog;

        /**
         * @ORM\Column(type="string", length=20, nullable=true)
         */
        private $image;

        /**
         * @ORM\Column(type="text")
         */
        private $tags;

        /**
         * @ORM\OneToMany(targetEntity="Comment", mappedBy="blog")
         */
        private $comments;

        /**
         * @ORM\Column(type="datetime")
         */
        private $created;

        /**
         * @ORM\Column(type="datetime")
         */
        private $updated;

        /**
         * @ORM\Column(type="string")
         */
        private $slug;

        public function __construct()
        {
            $this->comments = new ArrayCollection();

            $this->setCreated(new \DateTime());
            $this->setUpdated(new \DateTime());
        }

        /**
         * @ORM\PreUpdate
         */
        public function setUpdatedValue()
        {
            $this->setUpdated(new \DateTime());
        }

        public function getId(): ?int
        {
            return $this->id;
        }

        public function getTitle()
        {
            return $this->title;
        }

        public function setTitle($title)
        {
            $this->title = $title;

            $this->setSlug($this->title);
        }

        public function getAuthor(): ?string
        {
            return $this->author;
        }

        public function setAuthor(string $author): self
        {
            $this->author = $author;

            return $this;
        }

        public function getBlog(): ?string
        {
            return $this->blog;
        }

        public function setBlog(string $blog): self
        {
            $this->blog = $blog;

            return $this;
        }

        public function getImage(): ?string
        {
            return $this->image;
        }

        public function setImage(?string $image): self
        {
            $this->image = $image;

            return $this;
        }

        public function getTags(): ?string
        {
            return $this->tags;
        }

        public function setTags(string $tags): self
        {
            $this->tags = $tags;

            return $this;
        }

        /**
         * @return Collection|Comment[]
         */
        public function getComments(): Collection
        {
            return $this->comments;
        }

        public function setComments(?string $comments): self
        {
            $this->comments = $comments;

            return $this;
        }

        public function getCreated(): ?\DateTimeInterface
        {
            return $this->created;
        }

        public function setCreated(\DateTimeInterface $created): self
        {
            $this->created = $created;

            return $this;
        }

        public function getUpdated(): ?\DateTimeInterface
        {
            return $this->updated;
        }

        public function setUpdated(\DateTimeInterface $updated): self
        {
            $this->updated = $updated;

            return $this;
        }

        public function addComment(Comment $comment): self
        {
            if (!$this->comments->contains($comment)) {
                $this->comments[] = $comment;
                $comment->setBlog($this);
            }

            return $this;
        }

        public function removeComment(Comment $comment): self
        {
            if ($this->comments->contains($comment)) {
                $this->comments->removeElement($comment);
                // set the owning side to null (unless already changed)
                if ($comment->getBlog() === $this) {
                    $comment->setBlog(null);
                }
            }

            return $this;
        }

        public function __toString()
        {
            return $this->getTitle();
        }

        public function getSlug()
        {
            return $this->slug;
        }

        public function setSlug(string $slug)
        {
            $this->slug = $this->slugify($slug);
        }

        public function slugify($text)
        {
            // replace non letter or digits by -
            $text = preg_replace('#[^\\pL\d]+#u', '-', $text);

            // trim
            $text = trim($text, '-');

            // transliterate
            if (function_exists('iconv')) {
                $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
            }

            // lowercase
            $text = strtolower($text);

            // remove unwanted characters
            $text = preg_replace('#[^-\w]+#', '', $text);

            if (empty($text)) {
                return 'n-a';
            }

            return $text;
        }
    }
